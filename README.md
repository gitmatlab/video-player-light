# minimalist-video-player

The **minimalist-video-player** is a lightweight web component optimized for integrating Vimeo and YouTube video playback with minimal CPU and bandwidth usage. It features an on-click loading mechanism that speeds up initial page loads, supporting better performance metrics such as Web Core Vitals. 

## Getting started

Build and start. The index.html shows an example integration.

## Credits

This software is based on "video-player-light", github repository: luwes/lite-vimeo-embed.
It handles the video splash screen differently. You can basically use any image (in a sensible ratio).
And it can also handle youtube videos.
