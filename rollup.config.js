import copy from 'rollup-plugin-copy'
import { terser } from 'rollup-plugin-terser'
import postcss from 'rollup-plugin-postcss'
import cssnano from 'cssnano'
const bundleSize = require('rollup-plugin-bundle-size')

export default {
  input: './src/minimalist-video-player.js',
  output: {
    file: './build/minimalist-video-player.min.js',
    format: 'es'
  },
  plugins: [
    terser(),
    bundleSize(),
    postcss({
      plugins: [cssnano]
    }),    
    copy({
      targets: [{ src: ['src/index.html', 'src/favicon.ico', 'src/video-splash.jpg'], dest: 'build' }]
    }),
  ]
};
